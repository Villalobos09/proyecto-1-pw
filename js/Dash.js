// variables globales

const formularioUI = document.querySelector('#crear');
const listaProductosUI = document.getElementById('imagenesDash');
let arrayProductos = [];

//funciones

const CrearProducto = (nombreP, descripcio, image, busc) =>{
	let item = {
		nombreP: nombreP,
		descripcio: descripcio,
		image: image,
		busc: busc
	}
	arrayProductos.push(item);
	
	return item;
}

const Guardar = () =>{
	localStorage.setItem('Products', JSON.stringify(arrayProductos));
	Pintar();
}

const Pintar = () => {
	listaProductosUI.innerHTML = '';
	arrayProductos = JSON.parce(localStorage.getItem('Products'));
	if(arrayProductos === null){
		arrayProductos = [];
	}else{
		arrayProductos.forEach(element =>{
			listaProductosUI.innerHTML += `<div class= "alert alert-danger" role = "alert">
        <div>
        <img src="https://images.philips.com/is/image/PhilipsConsumer/275C5QHGSW_00-IMS-es_ES?$jpglarge$&wid=1250" width="150" height="150" alt="imagen"/>
            <b>${element.nombreP}</b>
        </div>
            <span>
                <i id="delete" class="material-icons">
                delete_forever
                </i>
                <i id="edit" class="material-icons">
                border_color
                </i>
            </span>

        </div>`
		});
	}
}


// EventListener

formularioUI.addEventListener('submit', (e)=>{

	e.preventDefault();
	let productUI = document.querySelector('#nombreP', '#descripcion', '#imagen', '#busca').value;

	CrearProducto(productUI);
	Guardar();
	formularioUI.reset();
});

document.addEventListener('DOMContentLoaded', Pintar());